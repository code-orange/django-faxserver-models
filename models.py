from django.db import models


class DataSipExtFax(models.Model):
    fax_id = models.AutoField(primary_key=True)
    user = models.ForeignKey("WpUsers", models.DO_NOTHING)
    fax_switch_ext_id = models.CharField(unique=True, max_length=30)
    dest_email = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = "data_sip_ext_fax"
